﻿Public Class Form5
    Public type As String
    Private Sub Form5_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If adminlogin.userRole = "employee" Then
            LinkLabel2.Hide() 'Hide add user
            Button5.Hide()
            calc_salary.Hide()
            salaryReport.Hide()
            AttendanceReportBtn.Hide()
        End If
        LinkLabel4.Hide()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        BookingList.Show()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        AttendanceForm.Show()
    End Sub

    Private Sub Button6_Click_1(sender As Object, e As EventArgs)
        remove.Show()
    End Sub

    Private Sub LinkLabel2_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles bookings.LinkClicked
        type = "booking"
        BookingList.Show()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        remove.Show()
    End Sub

    Private Sub LinkLabel3_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel3.LinkClicked
        type = "invoice"
        BookingList.Show()
    End Sub

    Private Sub LinkLabel2_LinkClicked_1(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked
        AddUser.Show()
    End Sub

    Private Sub attendanceButon_clciked(sender As Object, e As LinkLabelLinkClickedEventArgs)

    End Sub

    Private Sub LinkLabel8_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel8.LinkClicked
        'Company details
        CompanySettings.Show()
    End Sub

    Private Sub calc_salary_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles calc_salary.LinkClicked
        'Calculate salary
        CalcSalary.Show()
    End Sub

    Private Sub QuotationToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles QuotationToolStripMenuItem.Click
        remove.Show()
    End Sub

    Private Sub AttendanceToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AttendanceToolStripMenuItem.Click
        AttendanceForm.Show()
    End Sub

    Private Sub CompanyDetailsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CompanyDetailsToolStripMenuItem.Click
        CompanySettings.Show()
    End Sub

    Private Sub InvoiceToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles InvoiceToolStripMenuItem.Click
        type = "invoice"
        BookingList.Show()
    End Sub

    Private Sub QuotationsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles QuotationsToolStripMenuItem.Click
        BookingList.Show()
    End Sub

    Private Sub AttendanceReport_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles AttendanceReportBtn.LinkClicked
        AttendanceReport.Show()
    End Sub

    Private Sub SalaryReport_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles salaryReport.LinkClicked
        ViewSalaryReport.Show()
    End Sub
End Class