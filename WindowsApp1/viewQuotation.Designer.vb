﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class viewQuotation
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.client_name_label = New System.Windows.Forms.Label()
        Me.client_name_value = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.venue = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.book_date = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.quotation_id = New System.Windows.Forms.Label()
        Me.itemDetail = New System.Windows.Forms.TableLayoutPanel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.total_cost = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.phone = New System.Windows.Forms.Label()
        Me.emailid = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'client_name_label
        '
        Me.client_name_label.AutoSize = True
        Me.client_name_label.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.client_name_label.Location = New System.Drawing.Point(59, 54)
        Me.client_name_label.Name = "client_name_label"
        Me.client_name_label.Size = New System.Drawing.Size(95, 17)
        Me.client_name_label.TabIndex = 0
        Me.client_name_label.Text = "Client Name"
        '
        'client_name_value
        '
        Me.client_name_value.AutoSize = True
        Me.client_name_value.Location = New System.Drawing.Point(160, 54)
        Me.client_name_value.Name = "client_name_value"
        Me.client_name_value.Size = New System.Drawing.Size(38, 17)
        Me.client_name_value.TabIndex = 0
        Me.client_name_value.Text = "cn_v"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(59, 89)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Venue"
        Me.Label1.UseWaitCursor = True
        '
        'venue
        '
        Me.venue.AutoSize = True
        Me.venue.Location = New System.Drawing.Point(160, 89)
        Me.venue.Name = "venue"
        Me.venue.Size = New System.Drawing.Size(38, 17)
        Me.venue.TabIndex = 0
        Me.venue.Text = "cn_v"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(59, 120)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 17)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Date"
        Me.Label2.UseWaitCursor = True
        '
        'book_date
        '
        Me.book_date.AutoSize = True
        Me.book_date.Location = New System.Drawing.Point(160, 120)
        Me.book_date.Name = "book_date"
        Me.book_date.Size = New System.Drawing.Size(38, 17)
        Me.book_date.TabIndex = 0
        Me.book_date.Text = "cn_v"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(59, 25)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(97, 17)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Quotation Id"
        '
        'quotation_id
        '
        Me.quotation_id.AutoSize = True
        Me.quotation_id.Location = New System.Drawing.Point(160, 25)
        Me.quotation_id.Name = "quotation_id"
        Me.quotation_id.Size = New System.Drawing.Size(38, 17)
        Me.quotation_id.TabIndex = 0
        Me.quotation_id.Text = "cn_v"
        '
        'itemDetail
        '
        Me.itemDetail.ColumnCount = 2
        Me.itemDetail.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.itemDetail.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.itemDetail.Location = New System.Drawing.Point(62, 261)
        Me.itemDetail.Name = "itemDetail"
        Me.itemDetail.RowCount = 2
        Me.itemDetail.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.itemDetail.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.itemDetail.Size = New System.Drawing.Size(200, 100)
        Me.itemDetail.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(59, 149)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(82, 17)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Total Cost"
        Me.Label4.UseWaitCursor = True
        '
        'total_cost
        '
        Me.total_cost.AutoSize = True
        Me.total_cost.Location = New System.Drawing.Point(160, 149)
        Me.total_cost.Name = "total_cost"
        Me.total_cost.Size = New System.Drawing.Size(38, 17)
        Me.total_cost.TabIndex = 0
        Me.total_cost.Text = "cn_v"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(59, 180)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(54, 17)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Phone"
        Me.Label5.UseWaitCursor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(59, 214)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(65, 17)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Email Id"
        Me.Label6.UseWaitCursor = True
        '
        'phone
        '
        Me.phone.AutoSize = True
        Me.phone.Location = New System.Drawing.Point(160, 180)
        Me.phone.Name = "phone"
        Me.phone.Size = New System.Drawing.Size(38, 17)
        Me.phone.TabIndex = 0
        Me.phone.Text = "cn_v"
        '
        'emailid
        '
        Me.emailid.AutoSize = True
        Me.emailid.Location = New System.Drawing.Point(160, 214)
        Me.emailid.Name = "emailid"
        Me.emailid.Size = New System.Drawing.Size(38, 17)
        Me.emailid.TabIndex = 0
        Me.emailid.Text = "cn_v"
        '
        'viewQuotation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(990, 579)
        Me.Controls.Add(Me.itemDetail)
        Me.Controls.Add(Me.emailid)
        Me.Controls.Add(Me.phone)
        Me.Controls.Add(Me.total_cost)
        Me.Controls.Add(Me.book_date)
        Me.Controls.Add(Me.venue)
        Me.Controls.Add(Me.quotation_id)
        Me.Controls.Add(Me.client_name_value)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.client_name_label)
        Me.Name = "viewQuotation"
        Me.Text = "viewQuotation"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents client_name_label As Label
    Friend WithEvents client_name_value As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents venue As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents book_date As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents quotation_id As Label
    Friend WithEvents itemDetail As TableLayoutPanel
    Friend WithEvents Label4 As Label
    Friend WithEvents total_cost As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents phone As Label
    Friend WithEvents emailid As Label
End Class
