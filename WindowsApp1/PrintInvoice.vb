﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Drawing.Printing

Public Class PrintInvoice
    Private fileName As String = "file"

    Private textToPrint As String = "
    	                 PulseSounds
        	             ===========
    "
    Private Sub PrintInvoice_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim bookid As String = BookingList.bookId
        Dim con As New SqlConnection
        Dim cmd As New SqlCommand

        invoice_num.Text = bookid
        con.ConnectionString = ConfigurationManager.ConnectionStrings("connectionString").ConnectionString
        con.Open()

        cmd.Connection = con
        cmd.CommandText = "SELECT bookid, client_name, 
                            venue, date, total_cost, email, phone FROM booking 
                            WHERE bookid = " & bookid
        Dim dbRec As SqlDataReader = cmd.ExecuteReader()
        If dbRec.HasRows Then
            dbRec.Read()
            Dim cname As String = dbRec("client_name").ToString()
            Dim venue1 As String = dbRec("venue").ToString()
            Dim email1 As String = dbRec("email").ToString()
            Dim phone1 As String = dbRec("phone").ToString()
            Dim tcost As String = "Rs:" & dbRec("total_cost").ToString()

            client_name_value.Text = cname
            venue.Text = venue1
            Dim dt As String = dbRec("date").ToString()
            invoice_date.Text = dt
            total_cost.Text = tcost
            email.Text = email1
            phone.Text = phone1

            fileName = cname & "_" & bookid & "_" & "invoice"
            textToPrint &= "  	
Inoice No: " & bookid & "

Date:" & dt &
"

Shop Address: 	Office No 2
    Amtady Panchayath Road
	Loretto Padav, Bantwal.
	574211

Client Name: " & cname & "
Venue: " & venue1 & "
Email: " & email1 & "
Phone: " & phone1 & "
Total Cost: " & tcost & "


"

        End If
        con.Close()

        con.Open()
        cmd.Connection = con
        cmd.CommandText = "SELECT * FROM booking_item WHERE bookid = " & bookid
        Dim tableRecord As SqlDataReader = cmd.ExecuteReader()
        itemDetail.AutoSize = True
        Dim i As Integer
        If tableRecord.HasRows Then
            itemDetail.ColumnCount = 4
            itemDetail.RowCount = 2

            'Generate table header
            Dim header = New String() {"Item name", "Quantity", "Price", "Total (Rs)"}
            Dim plchldr = New Integer() {12, 12, 9, 12}
            textToPrint &= "| "
            For i = 0 To 3
                Dim val As String = header(i)
                textToPrint &= val.PadRight(plchldr(i), " ") & " | "
                itemDetail.ColumnStyles.Add(New ColumnStyle(SizeType.AutoSize))
                Dim lbl As Label = New Label()
                lbl.Text = val
                lbl.Font = New Font(Label2.Font, FontStyle.Bold)
                itemDetail.Controls.Add(lbl, i, 0)
            Next
            textToPrint &= "
"
            textToPrint &= "".PadRight(plchldr.Sum() + 13, "_")
            textToPrint &= "
"
            While tableRecord.Read()
                textToPrint &= "| "
                itemDetail.RowStyles.Add(New RowStyle(SizeType.AutoSize))
                For i = 2 To 5
                    Dim val As String = tableRecord(i).ToString
                    itemDetail.ColumnStyles.Add(New ColumnStyle(SizeType.AutoSize))
                    Dim lbl As Label = New Label()
                    lbl.Text = val
                    textToPrint &= val.PadRight(plchldr(i - 2), " ") & " | "
                    itemDetail.Controls.Add(lbl, i - 2, itemDetail.RowCount)
                Next
                textToPrint &= "
"
                itemDetail.RowCount += 1
            End While

            textToPrint &= "".PadRight(plchldr.Sum() + 13, "_")
            con.Close()
        Else
            MsgBox("Not Items found in this booking!!")
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Printer.Text.Print(textToPrint, fileName)
    End Sub
End Class

Namespace Printer
    Public Module Text

        Private defaultFont As Font = New Font("Consolas", 12)

        Private pDocument As PrintDocument
        Private pData As String

        Public Sub Print(ByVal text As String, ByVal fileName As String)
            'Create a new instance of the PrintDocument object
            pDocument = New PrintDocument
            pDocument.DocumentName = fileName
            'Create the event handler to print text using the default font
            AddHandler pDocument.PrintPage, AddressOf pDocument_PrintPage

            'Set the print data to the text
            pData = text

            'Print!
            pDocument.Print()
        End Sub

        Private Sub pDocument_PrintPage(ByVal sender As Object, ByVal e As PrintPageEventArgs)
            'Get the text, the amount of characters on the page, and the amount of lines per page
            Dim charactersOnPage As Integer = 0
            Dim linesPerPage As Integer = 0

            'Sets the value of charactersOnPage to the number of characters of s that will fit within the bounds of the page.
            e.Graphics.MeasureString(pData, defaultFont, e.MarginBounds.Size, StringFormat.GenericTypographic, charactersOnPage, linesPerPage)

            'Draws the string within the bounds of the page
            e.Graphics.DrawString(pData, defaultFont, Brushes.Black, e.MarginBounds, StringFormat.GenericTypographic)

            'Remove the portion of the string that has been printed.
            pData = pData.Substring(charactersOnPage)

            'Check to see if more pages are to be printed.
            e.HasMorePages = (pData.Length > 0)
        End Sub
    End Module
End Namespace