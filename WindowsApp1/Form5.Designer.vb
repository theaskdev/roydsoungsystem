﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form5
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form5))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.NewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuotationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AttendanceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CompanyDetailsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InvoiceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuotationsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.bookings = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel3 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel4 = New System.Windows.Forms.LinkLabel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.LinkLabel8 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel2 = New System.Windows.Forms.LinkLabel()
        Me.calc_salary = New System.Windows.Forms.LinkLabel()
        Me.AttendanceReportBtn = New System.Windows.Forms.LinkLabel()
        Me.salaryReport = New System.Windows.Forms.LinkLabel()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.Black
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewToolStripMenuItem, Me.SettingsToolStripMenuItem, Me.ReportsToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(8, 2, 0, 2)
        Me.MenuStrip1.Size = New System.Drawing.Size(1067, 28)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'NewToolStripMenuItem
        '
        Me.NewToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.QuotationToolStripMenuItem, Me.AttendanceToolStripMenuItem})
        Me.NewToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.NewToolStripMenuItem.Name = "NewToolStripMenuItem"
        Me.NewToolStripMenuItem.Size = New System.Drawing.Size(51, 24)
        Me.NewToolStripMenuItem.Text = "New"
        '
        'QuotationToolStripMenuItem
        '
        Me.QuotationToolStripMenuItem.Name = "QuotationToolStripMenuItem"
        Me.QuotationToolStripMenuItem.Size = New System.Drawing.Size(230, 26)
        Me.QuotationToolStripMenuItem.Text = "Quotation"
        '
        'AttendanceToolStripMenuItem
        '
        Me.AttendanceToolStripMenuItem.Name = "AttendanceToolStripMenuItem"
        Me.AttendanceToolStripMenuItem.Size = New System.Drawing.Size(230, 26)
        Me.AttendanceToolStripMenuItem.Text = "Employee Attendance"
        '
        'SettingsToolStripMenuItem
        '
        Me.SettingsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CompanyDetailsToolStripMenuItem})
        Me.SettingsToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.SettingsToolStripMenuItem.Name = "SettingsToolStripMenuItem"
        Me.SettingsToolStripMenuItem.Size = New System.Drawing.Size(74, 24)
        Me.SettingsToolStripMenuItem.Text = "Settings"
        '
        'CompanyDetailsToolStripMenuItem
        '
        Me.CompanyDetailsToolStripMenuItem.Name = "CompanyDetailsToolStripMenuItem"
        Me.CompanyDetailsToolStripMenuItem.Size = New System.Drawing.Size(197, 26)
        Me.CompanyDetailsToolStripMenuItem.Text = "Company Details"
        '
        'ReportsToolStripMenuItem
        '
        Me.ReportsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.InvoiceToolStripMenuItem, Me.QuotationsToolStripMenuItem})
        Me.ReportsToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.ReportsToolStripMenuItem.Name = "ReportsToolStripMenuItem"
        Me.ReportsToolStripMenuItem.Size = New System.Drawing.Size(72, 24)
        Me.ReportsToolStripMenuItem.Text = "Reports"
        '
        'InvoiceToolStripMenuItem
        '
        Me.InvoiceToolStripMenuItem.Name = "InvoiceToolStripMenuItem"
        Me.InvoiceToolStripMenuItem.Size = New System.Drawing.Size(157, 26)
        Me.InvoiceToolStripMenuItem.Text = "Invoice"
        '
        'QuotationsToolStripMenuItem
        '
        Me.QuotationsToolStripMenuItem.Name = "QuotationsToolStripMenuItem"
        Me.QuotationsToolStripMenuItem.Size = New System.Drawing.Size(157, 26)
        Me.QuotationsToolStripMenuItem.Text = "Quotations"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Navy
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(143, 126)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(304, 110)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "NEW BOOKING"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.Navy
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.Location = New System.Drawing.Point(316, 260)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(131, 46)
        Me.Button4.TabIndex = 4
        Me.Button4.Text = "QUOTATION"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.Navy
        Me.Button5.ForeColor = System.Drawing.Color.White
        Me.Button5.Location = New System.Drawing.Point(143, 260)
        Me.Button5.Margin = New System.Windows.Forms.Padding(4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(131, 46)
        Me.Button5.TabIndex = 5
        Me.Button5.Text = "ATTENDANCE"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Location = New System.Drawing.Point(577, 172)
        Me.LinkLabel1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(0, 17)
        Me.LinkLabel1.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(576, 112)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(127, 25)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "REPORTS :"
        '
        'bookings
        '
        Me.bookings.ActiveLinkColor = System.Drawing.Color.RosyBrown
        Me.bookings.AutoSize = True
        Me.bookings.BackColor = System.Drawing.Color.Transparent
        Me.bookings.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bookings.ForeColor = System.Drawing.Color.Transparent
        Me.bookings.LinkColor = System.Drawing.Color.DeepSkyBlue
        Me.bookings.Location = New System.Drawing.Point(576, 149)
        Me.bookings.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.bookings.Name = "bookings"
        Me.bookings.Size = New System.Drawing.Size(94, 23)
        Me.bookings.TabIndex = 10
        Me.bookings.TabStop = True
        Me.bookings.Text = "Bookings"
        '
        'LinkLabel3
        '
        Me.LinkLabel3.AutoSize = True
        Me.LinkLabel3.BackColor = System.Drawing.Color.Transparent
        Me.LinkLabel3.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel3.ForeColor = System.Drawing.Color.Transparent
        Me.LinkLabel3.LinkColor = System.Drawing.Color.LightBlue
        Me.LinkLabel3.LinkVisited = True
        Me.LinkLabel3.Location = New System.Drawing.Point(576, 188)
        Me.LinkLabel3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LinkLabel3.Name = "LinkLabel3"
        Me.LinkLabel3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LinkLabel3.Size = New System.Drawing.Size(84, 23)
        Me.LinkLabel3.TabIndex = 9
        Me.LinkLabel3.TabStop = True
        Me.LinkLabel3.Text = "Invoices"
        Me.LinkLabel3.VisitedLinkColor = System.Drawing.Color.DeepSkyBlue
        '
        'LinkLabel4
        '
        Me.LinkLabel4.AutoSize = True
        Me.LinkLabel4.BackColor = System.Drawing.Color.Transparent
        Me.LinkLabel4.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel4.LinkColor = System.Drawing.Color.DeepSkyBlue
        Me.LinkLabel4.Location = New System.Drawing.Point(576, 230)
        Me.LinkLabel4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LinkLabel4.Name = "LinkLabel4"
        Me.LinkLabel4.Size = New System.Drawing.Size(72, 23)
        Me.LinkLabel4.TabIndex = 10
        Me.LinkLabel4.TabStop = True
        Me.LinkLabel4.Text = "Clients"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(576, 300)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(134, 25)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "SETTINGS :"
        '
        'LinkLabel8
        '
        Me.LinkLabel8.AutoSize = True
        Me.LinkLabel8.BackColor = System.Drawing.Color.Transparent
        Me.LinkLabel8.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel8.ForeColor = System.Drawing.Color.Transparent
        Me.LinkLabel8.LinkColor = System.Drawing.Color.DeepSkyBlue
        Me.LinkLabel8.Location = New System.Drawing.Point(577, 349)
        Me.LinkLabel8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LinkLabel8.Name = "LinkLabel8"
        Me.LinkLabel8.Size = New System.Drawing.Size(163, 23)
        Me.LinkLabel8.TabIndex = 16
        Me.LinkLabel8.TabStop = True
        Me.LinkLabel8.Text = "Company Details"
        '
        'LinkLabel2
        '
        Me.LinkLabel2.AutoSize = True
        Me.LinkLabel2.BackColor = System.Drawing.Color.Transparent
        Me.LinkLabel2.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel2.ForeColor = System.Drawing.Color.Transparent
        Me.LinkLabel2.LinkColor = System.Drawing.Color.DeepSkyBlue
        Me.LinkLabel2.Location = New System.Drawing.Point(578, 395)
        Me.LinkLabel2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LinkLabel2.Name = "LinkLabel2"
        Me.LinkLabel2.Size = New System.Drawing.Size(142, 23)
        Me.LinkLabel2.TabIndex = 16
        Me.LinkLabel2.TabStop = True
        Me.LinkLabel2.Text = "Add Employee"
        '
        'calc_salary
        '
        Me.calc_salary.AutoSize = True
        Me.calc_salary.BackColor = System.Drawing.Color.Transparent
        Me.calc_salary.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.calc_salary.ForeColor = System.Drawing.Color.Transparent
        Me.calc_salary.LinkColor = System.Drawing.Color.DeepSkyBlue
        Me.calc_salary.Location = New System.Drawing.Point(578, 434)
        Me.calc_salary.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.calc_salary.Name = "calc_salary"
        Me.calc_salary.Size = New System.Drawing.Size(156, 23)
        Me.calc_salary.TabIndex = 16
        Me.calc_salary.TabStop = True
        Me.calc_salary.Text = "Calculate Salary"
        '
        'AttendanceReportBtn
        '
        Me.AttendanceReportBtn.ActiveLinkColor = System.Drawing.Color.RosyBrown
        Me.AttendanceReportBtn.AutoSize = True
        Me.AttendanceReportBtn.BackColor = System.Drawing.Color.Transparent
        Me.AttendanceReportBtn.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AttendanceReportBtn.ForeColor = System.Drawing.Color.Transparent
        Me.AttendanceReportBtn.LinkColor = System.Drawing.Color.DeepSkyBlue
        Me.AttendanceReportBtn.Location = New System.Drawing.Point(733, 149)
        Me.AttendanceReportBtn.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.AttendanceReportBtn.Name = "AttendanceReportBtn"
        Me.AttendanceReportBtn.Size = New System.Drawing.Size(182, 23)
        Me.AttendanceReportBtn.TabIndex = 10
        Me.AttendanceReportBtn.TabStop = True
        Me.AttendanceReportBtn.Text = "Attendance Report"
        '
        'salaryReport
        '
        Me.salaryReport.ActiveLinkColor = System.Drawing.Color.RosyBrown
        Me.salaryReport.AutoSize = True
        Me.salaryReport.BackColor = System.Drawing.Color.Transparent
        Me.salaryReport.Font = New System.Drawing.Font("Cambria", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.salaryReport.ForeColor = System.Drawing.Color.Transparent
        Me.salaryReport.LinkColor = System.Drawing.Color.DeepSkyBlue
        Me.salaryReport.Location = New System.Drawing.Point(733, 188)
        Me.salaryReport.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.salaryReport.Name = "salaryReport"
        Me.salaryReport.Size = New System.Drawing.Size(135, 23)
        Me.salaryReport.TabIndex = 10
        Me.salaryReport.TabStop = True
        Me.salaryReport.Text = "Salary Report"
        '
        'Form5
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Tan
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1067, 554)
        Me.Controls.Add(Me.calc_salary)
        Me.Controls.Add(Me.LinkLabel2)
        Me.Controls.Add(Me.LinkLabel8)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.LinkLabel4)
        Me.Controls.Add(Me.LinkLabel3)
        Me.Controls.Add(Me.salaryReport)
        Me.Controls.Add(Me.AttendanceReportBtn)
        Me.Controls.Add(Me.bookings)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.LinkLabel1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.ForeColor = System.Drawing.Color.White
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "Form5"
        Me.Text = "Pulse Sounds Bantwal-Mangalore"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents NewToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents QuotationToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SettingsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CompanyDetailsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ReportsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AttendanceToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents InvoiceToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents QuotationsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Button1 As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents Button5 As Button
    Friend WithEvents LinkLabel1 As LinkLabel
    Friend WithEvents Label1 As Label
    Friend WithEvents bookings As LinkLabel
    Friend WithEvents LinkLabel3 As LinkLabel
    Friend WithEvents LinkLabel4 As LinkLabel
    Friend WithEvents Label3 As Label
    Friend WithEvents LinkLabel8 As LinkLabel
    Friend WithEvents LinkLabel2 As LinkLabel
    Friend WithEvents calc_salary As LinkLabel
    Friend WithEvents AttendanceReportBtn As LinkLabel
    Friend WithEvents salaryReport As LinkLabel
End Class
