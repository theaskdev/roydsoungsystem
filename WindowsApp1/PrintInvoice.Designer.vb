﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PrintInvoice
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim shop_address As System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label()
        Me.invoice_num = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.invoice_date = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.itemDetail = New System.Windows.Forms.TableLayoutPanel()
        Me.client_name_label = New System.Windows.Forms.Label()
        Me.client_name_value = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.venue = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.total_cost = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.email = New System.Windows.Forms.Label()
        Me.phone = New System.Windows.Forms.Label()
        shop_address = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'shop_address
        '
        shop_address.AutoSize = True
        shop_address.Location = New System.Drawing.Point(281, 119)
        shop_address.Name = "shop_address"
        shop_address.Size = New System.Drawing.Size(105, 17)
        shop_address.TabIndex = 2
        shop_address.Text = "Shop Address: "
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(281, 86)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 17)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Invoice No:"
        '
        'invoice_num
        '
        Me.invoice_num.AutoSize = True
        Me.invoice_num.Location = New System.Drawing.Point(391, 86)
        Me.invoice_num.Name = "invoice_num"
        Me.invoice_num.Size = New System.Drawing.Size(13, 17)
        Me.invoice_num.TabIndex = 1
        Me.invoice_num.Text = "-"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(511, 86)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(42, 17)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Date:"
        '
        'invoice_date
        '
        Me.invoice_date.AutoSize = True
        Me.invoice_date.Location = New System.Drawing.Point(559, 86)
        Me.invoice_date.Name = "invoice_date"
        Me.invoice_date.Size = New System.Drawing.Size(13, 17)
        Me.invoice_date.TabIndex = 1
        Me.invoice_date.Text = "-"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(432, 119)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(83, 17)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Office No: 2"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(432, 136)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(172, 17)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Amtady Panchayath Road"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(432, 153)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(158, 17)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Loretto Padav, Bantwal."
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(432, 170)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(56, 17)
        Me.Label7.TabIndex = 3
        Me.Label7.Text = "574211"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(389, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(156, 29)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "PulseSounds"
        '
        'itemDetail
        '
        Me.itemDetail.ColumnCount = 2
        Me.itemDetail.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.itemDetail.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.itemDetail.Location = New System.Drawing.Point(284, 393)
        Me.itemDetail.Name = "itemDetail"
        Me.itemDetail.RowCount = 2
        Me.itemDetail.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.itemDetail.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.itemDetail.Size = New System.Drawing.Size(320, 100)
        Me.itemDetail.TabIndex = 4
        '
        'client_name_label
        '
        Me.client_name_label.AutoSize = True
        Me.client_name_label.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.client_name_label.Location = New System.Drawing.Point(281, 215)
        Me.client_name_label.Name = "client_name_label"
        Me.client_name_label.Size = New System.Drawing.Size(100, 17)
        Me.client_name_label.TabIndex = 5
        Me.client_name_label.Text = "Client Name:"
        '
        'client_name_value
        '
        Me.client_name_value.AutoSize = True
        Me.client_name_value.Location = New System.Drawing.Point(387, 215)
        Me.client_name_value.Name = "client_name_value"
        Me.client_name_value.Size = New System.Drawing.Size(13, 17)
        Me.client_name_value.TabIndex = 1
        Me.client_name_value.Text = "-"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(281, 241)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(59, 17)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Venue:"
        Me.Label8.UseWaitCursor = True
        '
        'venue
        '
        Me.venue.AutoSize = True
        Me.venue.Location = New System.Drawing.Point(387, 241)
        Me.venue.Name = "venue"
        Me.venue.Size = New System.Drawing.Size(13, 17)
        Me.venue.TabIndex = 1
        Me.venue.Text = "-"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(281, 269)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(82, 17)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "Total Cost"
        Me.Label9.UseWaitCursor = True
        '
        'total_cost
        '
        Me.total_cost.AutoSize = True
        Me.total_cost.Location = New System.Drawing.Point(387, 269)
        Me.total_cost.Name = "total_cost"
        Me.total_cost.Size = New System.Drawing.Size(13, 17)
        Me.total_cost.TabIndex = 1
        Me.total_cost.Text = "-"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(671, 34)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 7
        Me.Button1.Text = "Print"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(281, 297)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(47, 17)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "Eamil"
        Me.Label10.UseWaitCursor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(281, 327)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(54, 17)
        Me.Label11.TabIndex = 6
        Me.Label11.Text = "Phone"
        Me.Label11.UseWaitCursor = True
        '
        'email
        '
        Me.email.AutoSize = True
        Me.email.Location = New System.Drawing.Point(387, 297)
        Me.email.Name = "email"
        Me.email.Size = New System.Drawing.Size(13, 17)
        Me.email.TabIndex = 1
        Me.email.Text = "-"
        '
        'phone
        '
        Me.phone.AutoSize = True
        Me.phone.Location = New System.Drawing.Point(387, 327)
        Me.phone.Name = "phone"
        Me.phone.Size = New System.Drawing.Size(13, 17)
        Me.phone.TabIndex = 1
        Me.phone.Text = "-"
        '
        'PrintInvoice
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(998, 679)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.client_name_label)
        Me.Controls.Add(Me.itemDetail)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(shop_address)
        Me.Controls.Add(Me.invoice_num)
        Me.Controls.Add(Me.invoice_date)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.phone)
        Me.Controls.Add(Me.email)
        Me.Controls.Add(Me.total_cost)
        Me.Controls.Add(Me.venue)
        Me.Controls.Add(Me.client_name_value)
        Me.Controls.Add(Me.Label2)
        Me.Name = "PrintInvoice"
        Me.Text = "PrintInvoice"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As Label
    Friend WithEvents invoice_num As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents invoice_date As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents itemDetail As TableLayoutPanel
    Friend WithEvents client_name_label As Label
    Friend WithEvents client_name_value As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents venue As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents total_cost As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents email As Label
    Friend WithEvents phone As Label
End Class
