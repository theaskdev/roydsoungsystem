﻿Imports System.Configuration
Imports System.Data.SqlClient

Public Class viewQuotation
    Private Sub Label1_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub viewQuotation_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If BookingList.viewType = "print" Then
            Me.BackColor = Color.FromArgb(255, 255, 255)
        End If

        Dim bookid As String = BookingList.bookId
        Dim con As New SqlConnection
        Dim cmd As New SqlCommand

        quotation_id.Text = bookid

        con.ConnectionString = ConfigurationManager.ConnectionStrings("connectionString").ConnectionString
        con.Open()

        cmd.Connection = con
        cmd.CommandText = "SELECT bookid, client_name, 
                            venue, date, total_cost, phone, email FROM booking 
                            WHERE bookid = " & bookid
        Dim dbRec As SqlDataReader = cmd.ExecuteReader()
        If dbRec.HasRows Then
            dbRec.Read()
            client_name_value.Text = dbRec("client_name").ToString()
            venue.Text = dbRec("venue").ToString()
            book_date.Text = dbRec("date").ToString()
            total_cost.Text = "Rs:" & dbRec("total_cost").ToString()
            phone.Text = dbRec("phone").ToString()
            emailid.Text = dbRec("email").ToString()
        End If
        con.Close()

        con.Open()
        cmd.Connection = con
        cmd.CommandText = "SELECT * FROM booking_item WHERE bookid = " & bookid
        Dim tableRecord As SqlDataReader = cmd.ExecuteReader()
        itemDetail.AutoSize = True
        Dim i As Integer
        If tableRecord.HasRows Then
            itemDetail.ColumnCount = 4
            itemDetail.RowCount = 2

            'Generate table header
            Dim header = New String() {"Item name", "Quantity", "Price", "Total (Rs)"}

            For i = 0 To 3
                itemDetail.ColumnStyles.Add(New ColumnStyle(SizeType.AutoSize))
                Dim lbl As Label = New Label()
                lbl.Text = header(i)
                lbl.Font = New Font(Label1.Font, FontStyle.Bold)
                itemDetail.Controls.Add(lbl, i, 0)
            Next

            While tableRecord.Read()

                itemDetail.RowStyles.Add(New RowStyle(SizeType.AutoSize))
                For i = 2 To 5
                    itemDetail.ColumnStyles.Add(New ColumnStyle(SizeType.AutoSize))
                    Dim lbl As Label = New Label()
                    lbl.Text = tableRecord(i).ToString
                    itemDetail.Controls.Add(lbl, i - 2, itemDetail.RowCount)
                Next

                itemDetail.RowCount += 1
            End While
            con.Close()
        Else
            MsgBox("Not Items found in this booking!!")
        End If
    End Sub
End Class