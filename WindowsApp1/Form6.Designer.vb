﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class remove
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(remove))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.client = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.venue = New System.Windows.Forms.TextBox()
        Me.save = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.itemTable = New System.Windows.Forms.TableLayoutPanel()
        Me.itemName0 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.quantity0 = New System.Windows.Forms.TextBox()
        Me.price0 = New System.Windows.Forms.TextBox()
        Me.total = New System.Windows.Forms.Label()
        Me.total0 = New System.Windows.Forms.Label()
        Me.addItemRow = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.phone = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.emailid = New System.Windows.Forms.TextBox()
        Me.itemTable.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Cambria", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(16, 25)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 22)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Client "
        '
        'client
        '
        Me.client.Location = New System.Drawing.Point(113, 25)
        Me.client.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.client.Multiline = True
        Me.client.Name = "client"
        Me.client.Size = New System.Drawing.Size(195, 26)
        Me.client.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Cambria", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(16, 62)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(63, 22)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Venue"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Cambria", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(19, 95)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(54, 22)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Date "
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Location = New System.Drawing.Point(113, 90)
        Me.DateTimePicker1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.DateTimePicker1.MinDate = New Date(2019, 3, 21, 20, 38, 24, 0)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(195, 27)
        Me.DateTimePicker1.TabIndex = 6
        Me.DateTimePicker1.Value = New Date(2019, 3, 21, 20, 38, 24, 0)
        '
        'StatusStrip1
        '
        Me.StatusStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 705)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Padding = New System.Windows.Forms.Padding(1, 0, 19, 0)
        Me.StatusStrip1.Size = New System.Drawing.Size(1370, 22)
        Me.StatusStrip1.TabIndex = 7
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(-25, -172)
        Me.TextBox2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(177, 27)
        Me.TextBox2.TabIndex = 8
        '
        'venue
        '
        Me.venue.Location = New System.Drawing.Point(113, 57)
        Me.venue.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.venue.Name = "venue"
        Me.venue.Size = New System.Drawing.Size(195, 27)
        Me.venue.TabIndex = 10
        '
        'save
        '
        Me.save.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.save.Font = New System.Drawing.Font("Cambria", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.save.ForeColor = System.Drawing.Color.White
        Me.save.Location = New System.Drawing.Point(233, 441)
        Me.save.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.save.Name = "save"
        Me.save.Size = New System.Drawing.Size(100, 52)
        Me.save.TabIndex = 12
        Me.save.Text = "Save"
        Me.save.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackgroundImage = CType(resources.GetObject("Button1.BackgroundImage"), System.Drawing.Image)
        Me.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button1.Location = New System.Drawing.Point(871, 441)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(112, 52)
        Me.Button1.TabIndex = 15
        Me.Button1.UseVisualStyleBackColor = True
        '
        'itemTable
        '
        Me.itemTable.ColumnCount = 4
        Me.itemTable.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.98649!))
        Me.itemTable.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.01351!))
        Me.itemTable.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 196.0!))
        Me.itemTable.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 103.0!))
        Me.itemTable.Controls.Add(Me.itemName0, 0, 1)
        Me.itemTable.Controls.Add(Me.Label2, 0, 0)
        Me.itemTable.Controls.Add(Me.Label3, 1, 0)
        Me.itemTable.Controls.Add(Me.Label6, 2, 0)
        Me.itemTable.Controls.Add(Me.quantity0, 1, 1)
        Me.itemTable.Controls.Add(Me.price0, 2, 1)
        Me.itemTable.Controls.Add(Me.total, 3, 0)
        Me.itemTable.Controls.Add(Me.total0, 3, 1)
        Me.itemTable.Location = New System.Drawing.Point(20, 219)
        Me.itemTable.Name = "itemTable"
        Me.itemTable.RowCount = 2
        Me.itemTable.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.itemTable.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.itemTable.Size = New System.Drawing.Size(734, 75)
        Me.itemTable.TabIndex = 16
        '
        'itemName0
        '
        Me.itemName0.Location = New System.Drawing.Point(3, 37)
        Me.itemName0.Name = "itemName0"
        Me.itemName0.Size = New System.Drawing.Size(114, 27)
        Me.itemName0.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(92, 20)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Item name"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(216, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(76, 20)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Quantity"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(437, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(49, 20)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Price"
        '
        'quantity0
        '
        Me.quantity0.Location = New System.Drawing.Point(216, 37)
        Me.quantity0.Name = "quantity0"
        Me.quantity0.Size = New System.Drawing.Size(118, 27)
        Me.quantity0.TabIndex = 0
        '
        'price0
        '
        Me.price0.Location = New System.Drawing.Point(437, 37)
        Me.price0.Name = "price0"
        Me.price0.Size = New System.Drawing.Size(107, 27)
        Me.price0.TabIndex = 0
        '
        'total
        '
        Me.total.AutoSize = True
        Me.total.Location = New System.Drawing.Point(633, 0)
        Me.total.Name = "total"
        Me.total.Size = New System.Drawing.Size(49, 20)
        Me.total.TabIndex = 1
        Me.total.Text = "Total"
        '
        'total0
        '
        Me.total0.AutoSize = True
        Me.total0.BackColor = System.Drawing.Color.Transparent
        Me.total0.Font = New System.Drawing.Font("Cambria", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.total0.ForeColor = System.Drawing.Color.Black
        Me.total0.Location = New System.Drawing.Point(634, 34)
        Me.total0.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.total0.Name = "total0"
        Me.total0.Size = New System.Drawing.Size(21, 22)
        Me.total0.TabIndex = 5
        Me.total0.Text = "0"
        '
        'addItemRow
        '
        Me.addItemRow.Location = New System.Drawing.Point(760, 219)
        Me.addItemRow.Name = "addItemRow"
        Me.addItemRow.Size = New System.Drawing.Size(61, 35)
        Me.addItemRow.TabIndex = 2
        Me.addItemRow.Text = "+"
        Me.addItemRow.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(760, 260)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(61, 34)
        Me.Button2.TabIndex = 17
        Me.Button2.Text = "-"
        Me.Button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Cambria", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(19, 129)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(65, 22)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Phone"
        '
        'phone
        '
        Me.phone.Location = New System.Drawing.Point(113, 123)
        Me.phone.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.phone.Name = "phone"
        Me.phone.Size = New System.Drawing.Size(195, 27)
        Me.phone.TabIndex = 10
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Cambria", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(19, 162)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(82, 22)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Email Id"
        '
        'emailid
        '
        Me.emailid.Location = New System.Drawing.Point(113, 157)
        Me.emailid.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.emailid.Name = "emailid"
        Me.emailid.Size = New System.Drawing.Size(195, 27)
        Me.emailid.TabIndex = 10
        '
        'remove
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 19.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1370, 727)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.itemTable)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.save)
        Me.Controls.Add(Me.emailid)
        Me.Controls.Add(Me.phone)
        Me.Controls.Add(Me.venue)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.addItemRow)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.client)
        Me.Controls.Add(Me.Label4)
        Me.Font = New System.Drawing.Font("Cambria", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Name = "remove"
        Me.Text = "Pulse Sounds-Booking "
        Me.itemTable.ResumeLayout(False)
        Me.itemTable.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents client As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents DateTimePicker1 As DateTimePicker
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents venue As TextBox
    Friend WithEvents save As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents itemTable As TableLayoutPanel
    Friend WithEvents itemName0 As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents quantity0 As TextBox
    Friend WithEvents price0 As TextBox
    Friend WithEvents addItemRow As Button
    Private WithEvents total As Label
    Friend WithEvents total0 As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents Label7 As Label
    Friend WithEvents phone As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents emailid As TextBox
End Class
