﻿Imports System.Configuration
Imports System.Data.SqlClient

Public Class CompanySettings

    Function checkForInt(val As String)
        Dim num As Integer
        Dim int_OK As Boolean
        Dim index As Integer
        int_OK = Integer.TryParse(val, num)
        If int_OK Then
            index = num
        Else
            index = -1
        End If
        Return index
    End Function

    Function createField(fname As String, fval As String)
        Dim connString As String
        connString = ConfigurationManager.ConnectionStrings("connectionString").ConnectionString

        Dim con As New SqlConnection
        Dim cmd As New SqlCommand
        con.ConnectionString = connString
        con.Open()
        cmd.Connection = con
        Dim query As String = "INSERT INTO settings (field_name, value) "
        query &= "VALUES ('" & fname & "','" & fval & "')"
        cmd.CommandText = query
        cmd.ExecuteNonQuery()
        MsgBox(fname & " added successfully")
        emp_salary.Text = ""
    End Function

    Function updateField(fname As String, fval As String)
        Dim connString As String
        connString = ConfigurationManager.ConnectionStrings("connectionString").ConnectionString

        Dim con As New SqlConnection
        Dim cmd As New SqlCommand
        con.ConnectionString = connString
        con.Open()
        cmd.Connection = con
        Dim query As String = "UPDATE settings SET value = '" & fval & "' "
        query &= "WHERE field_name = '" & fname & "'"
        cmd.CommandText = query
        cmd.ExecuteNonQuery()
        MsgBox(fname & " updated successfully")
        emp_salary.Text = ""
    End Function

    Private Sub save_emp_salary_Click(sender As Object, e As EventArgs) Handles save_emp_salary.Click
        Dim salary As String = emp_salary.Text
        Dim val As Integer = checkForInt(salary)
        If val <> -1 Then
            Dim connString As String
            connString = ConfigurationManager.ConnectionStrings("connectionString").ConnectionString

            Dim con As New SqlConnection
            Dim cmd As New SqlCommand
            con.ConnectionString = connString
            con.Open()
            cmd.Connection = con
            cmd.CommandText = "SELECt * FROM settings WHERE field_name='salary'"
            Dim tableRecord As SqlDataReader = cmd.ExecuteReader()
            If tableRecord.HasRows Then
                updateField("salary", val)
            Else
                createField("salary", val)
            End If
        Else
                MsgBox("Enter valid salary")
        End If
    End Sub

    Function getSalaryPerDay()
        Dim sal As String = ""

        Dim con As New SqlConnection
        Dim cmd As New SqlCommand
        Dim query As String

        con.ConnectionString = ConfigurationManager.ConnectionStrings("connectionString").ConnectionString
        con.Open()
        cmd.Connection = con

        query = "SELECT value FROM settings WHERE field_name = 'salary'"
        cmd.CommandText = query
        Dim tableRecord As SqlDataReader = cmd.ExecuteReader()
        If tableRecord.HasRows Then
            tableRecord.Read()
            sal = tableRecord(0).ToString
        End If
        con.Close()

        Return sal
    End Function

    Private Sub CompanySettings_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        emp_salary.Text = getSalaryPerDay()
    End Sub
End Class