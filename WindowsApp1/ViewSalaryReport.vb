﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Drawing.Printing

Public Class ViewSalaryReport
    Private Sub ViewSalaryReport_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        report_table.AutoSize = True

        report_table.Controls.Clear()
        report_table.ColumnStyles.Clear()
        report_table.RowStyles.Clear()

        Dim con As New SqlConnection
        Dim cmd As New SqlCommand


        con.ConnectionString = ConfigurationManager.ConnectionStrings("connectionString").ConnectionString
        con.Open()
        cmd.Connection = con
        cmd.CommandText = "SELECT date, u.name, from_date, to_date, days, amount FROM salary_transaction s left join users u on u.uid = s.empid"
        'MsgBox(cmd.CommandText)

        Dim tableRecord As SqlDataReader = cmd.ExecuteReader()
        Dim i As Integer
        If tableRecord.HasRows Then
            report_table.ColumnCount = 6
            report_table.RowCount = 2
            'Generate table header
            Dim header = New String() {"Date", "Emp Name", "From date", "To Date", "Days", "Amount"}

            For i = 0 To header.Length - 1
                report_table.ColumnStyles.Add(New ColumnStyle(SizeType.AutoSize))
                Dim lbl As Label = New Label()
                lbl.Text = header(i)
                report_table.Controls.Add(lbl, i, 0)
            Next

            While tableRecord.Read()
                report_table.RowStyles.Add(New RowStyle(SizeType.AutoSize))
                report_table.ColumnStyles.Add(New ColumnStyle(SizeType.AutoSize))

                For i = 0 To 5
                    report_table.ColumnStyles.Add(New ColumnStyle(SizeType.AutoSize))
                    Dim lbl As Label = New Label()
                    lbl.Text = tableRecord(i).ToString
                    report_table.Controls.Add(lbl, i, report_table.RowCount)
                Next

                report_table.RowCount += 1
            End While
        End If
    End Sub

End Class