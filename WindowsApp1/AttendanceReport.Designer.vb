﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AttendanceReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.date_from = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.date_to = New System.Windows.Forms.DateTimePicker()
        Me.load_report = New System.Windows.Forms.Button()
        Me.report_table = New System.Windows.Forms.TableLayoutPanel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.employee = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(462, 35)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(210, 29)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Attencance Report"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(402, 102)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 17)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "From:"
        '
        'date_from
        '
        Me.date_from.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date_from.Location = New System.Drawing.Point(501, 97)
        Me.date_from.Name = "date_from"
        Me.date_from.Size = New System.Drawing.Size(200, 22)
        Me.date_from.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(402, 130)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(29, 17)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "To:"
        '
        'date_to
        '
        Me.date_to.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.date_to.Location = New System.Drawing.Point(501, 125)
        Me.date_to.Name = "date_to"
        Me.date_to.Size = New System.Drawing.Size(200, 22)
        Me.date_to.TabIndex = 2
        '
        'load_report
        '
        Me.load_report.Location = New System.Drawing.Point(405, 203)
        Me.load_report.Name = "load_report"
        Me.load_report.Size = New System.Drawing.Size(296, 23)
        Me.load_report.TabIndex = 3
        Me.load_report.Text = "Fetch report"
        Me.load_report.UseVisualStyleBackColor = True
        '
        'report_table
        '
        Me.report_table.ColumnCount = 2
        Me.report_table.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.report_table.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.report_table.Location = New System.Drawing.Point(408, 253)
        Me.report_table.Name = "report_table"
        Me.report_table.RowCount = 2
        Me.report_table.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.report_table.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.report_table.Size = New System.Drawing.Size(293, 97)
        Me.report_table.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(405, 162)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(70, 17)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Employee"
        '
        'employee
        '
        Me.employee.FormattingEnabled = True
        Me.employee.Location = New System.Drawing.Point(501, 159)
        Me.employee.Name = "employee"
        Me.employee.Size = New System.Drawing.Size(200, 24)
        Me.employee.TabIndex = 6
        '
        'AttendanceReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1080, 629)
        Me.Controls.Add(Me.employee)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.report_table)
        Me.Controls.Add(Me.load_report)
        Me.Controls.Add(Me.date_to)
        Me.Controls.Add(Me.date_from)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "AttendanceReport"
        Me.Text = "AttendanceReport"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents date_from As DateTimePicker
    Friend WithEvents Label3 As Label
    Friend WithEvents date_to As DateTimePicker
    Friend WithEvents load_report As Button
    Friend WithEvents report_table As TableLayoutPanel
    Friend WithEvents Label4 As Label
    Friend WithEvents employee As ComboBox
End Class
