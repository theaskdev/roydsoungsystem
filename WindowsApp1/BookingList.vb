﻿Imports System.Configuration
Imports System.Data.SqlClient

Public Class BookingList
    Public bookId As String
    Public viewType As String = ""
    Private Sub TableLayoutPanel1_Paint(sender As Object, e As PaintEventArgs)

    End Sub

    Function loadBooking()
        'TODO: This line of code loads data into the 'PulsesoundsDataSet.booking' table. You can move, or remove it, as needed.
        BookingListTable.AutoSize = True
        Dim con As New SqlConnection
        Dim cmd As New SqlCommand

        Dim status As String = "booking"
        If Form5.type <> "" Then
            status = Form5.type
        End If

        con.ConnectionString = ConfigurationManager.ConnectionStrings("connectionString").ConnectionString
        con.Open()

        cmd.Connection = con
        cmd.CommandText = "SELECT bookid, client_name, venue, date, total_cost FROM booking WHERE status = '" & status & "'"

        BookingListTable.Controls.Clear()
        BookingListTable.ColumnStyles.Clear()
        BookingListTable.RowStyles.Clear()

        Dim tableRecord As SqlDataReader = cmd.ExecuteReader()

        Dim i As Integer
        If tableRecord.HasRows Then
            BookingListTable.ColumnCount = 11
            BookingListTable.RowCount = 2

            'Generate table header
            Dim header = New String() {"ID", "Client name", "Venue", "Date", "Total Cost", "Actions"}

            For i = 0 To header.Length - 1
                BookingListTable.ColumnStyles.Add(New ColumnStyle(SizeType.AutoSize))
                Dim lbl As Label = New Label()
                lbl.Text = header(i)
                BookingListTable.Controls.Add(lbl, i, 0)
            Next

            While tableRecord.Read()

                BookingListTable.RowStyles.Add(New RowStyle(SizeType.AutoSize))
                For i = 0 To 4
                    BookingListTable.ColumnStyles.Add(New ColumnStyle(SizeType.AutoSize))
                    Dim lbl As Label = New Label()
                    lbl.Text = tableRecord(i).ToString
                    BookingListTable.Controls.Add(lbl, i, BookingListTable.RowCount)
                Next

                Dim delete As Button = New Button()
                delete.Text = "Delete"
                BookingListTable.Controls.Add(delete, 5, BookingListTable.RowCount)
                AddHandler delete.Click, AddressOf Delete_Click

                Dim edit As Button = New Button()
                edit.Text = "Edit"
                BookingListTable.Controls.Add(edit, 6, BookingListTable.RowCount)
                AddHandler edit.Click, AddressOf edit_Click

                Dim view As Button = New Button()
                view.Text = "View items"
                BookingListTable.Controls.Add(view, 7, BookingListTable.RowCount)
                AddHandler view.Click, AddressOf view_Click

                If status <> "invoice" Then
                    Dim inv As Button = New Button()
                    inv.AutoSize = True
                    inv.Text = "Save as invoice"
                    BookingListTable.Controls.Add(inv, 8, BookingListTable.RowCount)
                    AddHandler inv.Click, AddressOf SaveAsInvoiceButton_Click
                End If

                Dim ViewQuotation As Button = New Button()
                ViewQuotation.AutoSize = True
                ViewQuotation.Text = "View Quotation"
                BookingListTable.Controls.Add(ViewQuotation, 9, BookingListTable.RowCount)
                AddHandler ViewQuotation.Click, AddressOf viewQuotationClicked

                If status = "invoice" Then
                    Dim print As Button = New Button()
                    print.AutoSize = True
                    print.Text = "Print"
                    BookingListTable.Controls.Add(print, 10, BookingListTable.RowCount)
                    AddHandler print.Click, AddressOf PrintInvoiceButton_Click
                End If

                BookingListTable.RowCount += 1
            End While
        Else
            MsgBox("Not bookings found!!")
        End If

        con.Close()
    End Function
    Private Sub BookingList_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Label1.Hide()
        loadBooking()
    End Sub

    Private Sub PrintInvoiceButton_Click(sender As Object, e As EventArgs)
        viewType = "print"
        Dim bookidLbl As Label = BookingListTable.GetControlFromPosition(0, BookingListTable.GetRow(sender))
        'Dim bookid As String = CType(bookidLbl.Text, String)
        bookId = CType(bookidLbl.Text, String)
        PrintInvoice.Show()
    End Sub

    Private Sub edit_Click(sender As Object, e As EventArgs)
        Dim bookidLbl As Label = BookingListTable.GetControlFromPosition(0, BookingListTable.GetRow(sender))
        bookId = CType(bookidLbl.Text, String)
        remove.Show()
    End Sub

    Private Sub view_Click(sender As Object, e As EventArgs)

        Dim bookidLbl As Label = BookingListTable.GetControlFromPosition(0, BookingListTable.GetRow(sender))

        Dim bookid As String = CType(bookidLbl.Text, String)
        itemDetail.AutoSize = True
        itemDetail.Controls.Clear()
        itemDetail.ColumnStyles.Clear()
        itemDetail.RowStyles.Clear()

        Dim con As New SqlConnection
        Dim cmd As New SqlCommand

        con.ConnectionString = ConfigurationManager.ConnectionStrings("connectionString").ConnectionString
        con.Open()

        cmd.Connection = con
        cmd.CommandText = "SELECT * FROM booking_item WHERE bookid = " & bookid

        Dim tableRecord As SqlDataReader = cmd.ExecuteReader()

        Dim i As Integer
        If tableRecord.HasRows Then
            itemDetail.ColumnCount = 4
            itemDetail.RowCount = 2

            'Generate table header
            Dim header = New String() {"Item name", "Quantity", "Price", "Total"}

            For i = 0 To 3
                itemDetail.ColumnStyles.Add(New ColumnStyle(SizeType.AutoSize))
                Dim lbl As Label = New Label()
                lbl.Text = header(i)
                lbl.Font = New Font(Label1.Font, FontStyle.Bold)
                itemDetail.Controls.Add(lbl, i, 0)
            Next

            While tableRecord.Read()

                itemDetail.RowStyles.Add(New RowStyle(SizeType.AutoSize))
                For i = 2 To 5
                    itemDetail.ColumnStyles.Add(New ColumnStyle(SizeType.AutoSize))
                    Dim lbl As Label = New Label()
                    lbl.Text = tableRecord(i).ToString
                    itemDetail.Controls.Add(lbl, i - 2, itemDetail.RowCount)
                Next

                itemDetail.RowCount += 1
            End While
            con.Close()
        Else
            MsgBox("Not Items found in this booking!!")
        End If
    End Sub

    Private Sub Delete_Click(sender As Object, e As EventArgs)
        Dim bookidLbl As Label = BookingListTable.GetControlFromPosition(0, BookingListTable.GetRow(sender))
        Dim bookid As String = CType(bookidLbl.Text, String)
        'If Label1.Text <> "" Then
        Dim result As Integer = MessageBox.Show("Are you sure to delete?", "Confirm", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            Dim con As New SqlConnection
            Dim cmd As New SqlCommand

            con.ConnectionString = ConfigurationManager.ConnectionStrings("connectionString").ConnectionString
            con.Open()
            cmd.Connection = con
            cmd.CommandText = "DELETE FROM booking_item WHERE bookid = " & bookid
            cmd.ExecuteNonQuery()

            cmd.CommandText = "DELETE FROM booking WHERE bookid = " & bookid
            cmd.ExecuteNonQuery()

            con.Close()
            
            itemDetail.Controls.Clear()
            itemDetail.ColumnStyles.Clear()
            itemDetail.RowStyles.Clear()

        End If
        'End If
        loadBooking()
    End Sub

    Private Sub SaveAsInvoiceButton_Click(sender As Object, e As EventArgs)

        Dim result As Integer = MessageBox.Show("Are you sure?", "Confirm", MessageBoxButtons.YesNo)
        If result = DialogResult.Yes Then
            Dim bookidLbl As Label = BookingListTable.GetControlFromPosition(0, BookingListTable.GetRow(sender))
            Dim bookid As String = CType(bookidLbl.Text, String)

            Dim con As New SqlConnection
            Dim cmd As New SqlCommand
            con.ConnectionString = ConfigurationManager.ConnectionStrings("connectionString").ConnectionString
            con.Open()
            cmd.Connection = con
            cmd.CommandText = "UPDATE booking SET status = 'invoice' WHERE bookid = " & bookid
            cmd.ExecuteNonQuery()
            con.Close()

            MsgBox("Successfully saved as invoice.")
            loadBooking()
        End If
    End Sub

    Private Sub viewQuotationClicked(sender As Object, e As EventArgs)
        Dim bookidLbl As Label = BookingListTable.GetControlFromPosition(0, BookingListTable.GetRow(sender))
        'Dim bookid As String = CType(bookidLbl.Text, String)
        bookId = CType(bookidLbl.Text, String)
        viewQuotation.Show()
    End Sub
End Class