﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AttendanceForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.employee = New System.Windows.Forms.ComboBox()
        Me.attendance_date = New System.Windows.Forms.DateTimePicker()
        Me.present = New System.Windows.Forms.RadioButton()
        Me.absent = New System.Windows.Forms.RadioButton()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'employee
        '
        Me.employee.FormattingEnabled = True
        Me.employee.Location = New System.Drawing.Point(119, 106)
        Me.employee.Name = "employee"
        Me.employee.Size = New System.Drawing.Size(154, 24)
        Me.employee.TabIndex = 0
        '
        'attendance_date
        '
        Me.attendance_date.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.attendance_date.Location = New System.Drawing.Point(323, 107)
        Me.attendance_date.MaxDate = New Date(2100, 12, 31, 0, 0, 0, 0)
        Me.attendance_date.Name = "attendance_date"
        Me.attendance_date.Size = New System.Drawing.Size(200, 22)
        Me.attendance_date.TabIndex = 1
        Me.attendance_date.Value = New Date(2019, 3, 21, 0, 0, 0, 0)
        '
        'present
        '
        Me.present.AutoSize = True
        Me.present.Location = New System.Drawing.Point(594, 108)
        Me.present.Name = "present"
        Me.present.Size = New System.Drawing.Size(78, 21)
        Me.present.TabIndex = 2
        Me.present.TabStop = True
        Me.present.Text = "Present"
        Me.present.UseVisualStyleBackColor = True
        '
        'absent
        '
        Me.absent.AutoSize = True
        Me.absent.Location = New System.Drawing.Point(744, 106)
        Me.absent.Name = "absent"
        Me.absent.Size = New System.Drawing.Size(73, 21)
        Me.absent.TabIndex = 2
        Me.absent.TabStop = True
        Me.absent.Text = "Absent"
        Me.absent.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(405, 206)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(182, 23)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Save Attendance"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(398, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(277, 39)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Attendance Form"
        '
        'AttendanceForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1018, 509)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.absent)
        Me.Controls.Add(Me.present)
        Me.Controls.Add(Me.attendance_date)
        Me.Controls.Add(Me.employee)
        Me.Name = "AttendanceForm"
        Me.Text = "AttendanceForm"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents employee As ComboBox
    Friend WithEvents attendance_date As DateTimePicker
    Friend WithEvents present As RadioButton
    Friend WithEvents absent As RadioButton
    Friend WithEvents Button1 As Button
    Friend WithEvents Label1 As Label
End Class
