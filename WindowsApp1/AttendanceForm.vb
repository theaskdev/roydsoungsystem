﻿Imports System.Configuration
Imports System.Data.SqlClient

Public Class AttendanceForm
    Private Sub AttendanceForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim con As New SqlConnection
        Dim cmd As New SqlCommand
        attendance_date.Text = Date.Now
        attendance_date.MaxDate = Date.Now
        con.ConnectionString = ConfigurationManager.ConnectionStrings("connectionString").ConnectionString
        con.Open()
        cmd.Connection = con
        cmd.CommandText = "SELECt * FROM users WHERE role='employee'"
        Dim tableRecord As SqlDataReader = cmd.ExecuteReader()
        If tableRecord.HasRows Then

            Dim comboSource As New Dictionary(Of String, String)()
            While tableRecord.Read()
                comboSource.Add(tableRecord("uid").ToString, tableRecord("name").ToString)
                employee.DataSource = New BindingSource(comboSource, Nothing)
                employee.DisplayMember = "Value"
                employee.ValueMember = "Key"
            End While
        End If
        con.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim empid As String = DirectCast(employee.SelectedItem, KeyValuePair(Of String, String)).Key

        Dim status As String = ""

        If absent.Checked Then
            status = "A"
        ElseIf present.Checked Then
            status = "P"
        Else
            MsgBox("Please select present or absent")
        End If

        If status <> "" Then

            Dim exist As Boolean = checkAlreadyAdded(attendance_date.Text, empid)
            If exist Then
                MsgBox("Attendance already added to this emplyee for this date")
                Return
            End If

            Dim con As New SqlConnection
            Dim cmd As New SqlCommand
            Dim query As String

            con.ConnectionString = ConfigurationManager.ConnectionStrings("connectionString").ConnectionString
            con.Open()
            cmd.Connection = con

            query = "INSERT INTO attendance (empid, status, date) "
            query &= "VALUES ('" & empid & "','" & status & "','" & attendance_date.Text & "')"
            cmd.CommandText = query
            cmd.ExecuteNonQuery()
            con.Close()

            MsgBox("Attendance added successfully.")
            Me.Close()
        End If
    End Sub

    Function checkAlreadyAdded(dateVal As String, empid As Integer)
        Dim stat As Boolean = False

        Dim con As New SqlConnection
        Dim cmd As New SqlCommand
        Dim query As String

        con.ConnectionString = ConfigurationManager.ConnectionStrings("connectionString").ConnectionString
        con.Open()
        cmd.Connection = con

        query = "SELECT * FROM attendance WHERE date = '" & dateVal & "' AND empid = " & empid
        cmd.CommandText = query
        Dim tableRecord As SqlDataReader = cmd.ExecuteReader()
        If tableRecord.HasRows Then
            stat = True
        End If
        con.Close()

        Return stat
    End Function
End Class