﻿Imports System.Configuration
Imports System.Data.SqlClient

Public Class AddUser
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If uname.Text = "" Then
            MsgBox("Please enter name.")
            Return
        ElseIf username.Text = "" Then
            MsgBox("Please enter user name.")
            Return
        ElseIf password.Text = "" Then
            MsgBox("Please enter password.")
            Return
        ElseIf password.Text <> confirm_password.Text Then
            MsgBox("Password and Confirm password must be same.")
            Return
        End If

        Dim connString As String = ConfigurationManager.ConnectionStrings("connectionString").ConnectionString
        Dim query As String = ""

        Dim con1, con2 As New SqlConnection
        Dim cmd1, cmd2 As New SqlCommand
        con1.ConnectionString = connString
        con1.Open()

        cmd1 = New SqlCommand("SELECT * FROM users WHERE username = '" & username.Text & "'", con1)
        Dim tableRecord As SqlDataReader = cmd1.ExecuteReader()

        If tableRecord.HasRows Then
            MsgBox("Username already used")
        Else
            con2.ConnectionString = connString
            con2.Open()
            query = "INSERT INTO users (name, username, password, role) "
            query &= "VALUES ('" & uname.Text & "','" & username.Text & "','" & password.Text & "', 'employee')"
            cmd2 = New SqlCommand(query, con2)
            cmd2.ExecuteNonQuery()
            MsgBox("User created successfully..")
            con2.Close()
            Me.Close()
        End If

        con1.Close()

    End Sub
End Class