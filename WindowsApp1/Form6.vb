﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions

Public Class remove
    Private bookIdFromOtherForm As String = 0

    Private Sub remove_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        bookIdFromOtherForm = BookingList.bookId
        DateTimePicker1.MinDate = Date.Today
        If bookIdFromOtherForm <> 0 Then
            Dim con As New SqlConnection
            Dim cmd As New SqlCommand

            Dim status As String = "booking"
            If Form5.type <> "" Then
                status = Form5.type
            End If

            con.ConnectionString = ConfigurationManager.ConnectionStrings("connectionString").ConnectionString
            con.Open()

            cmd.Connection = con
            cmd.CommandText = "SELECT bookid, client_name, venue, date, total_cost FROM booking WHERE bookid = " & bookIdFromOtherForm

            Dim tableRecord As SqlDataReader = cmd.ExecuteReader()
            If tableRecord.HasRows Then
                tableRecord.Read()
                client.Text = tableRecord("client_name").ToString
                venue.Text = tableRecord("venue").ToString
                Dim dateFromdb As Date = Format(tableRecord("date").ToString, "Long Date")
                DateTimePicker1.Value = dateFromdb
            End If
            con.Close()

            con.Open()
            cmd.Connection = con
            cmd.CommandText = "SELECT * FROM booking_item WHERE bookid = " & bookIdFromOtherForm
            tableRecord = cmd.ExecuteReader()
            If tableRecord.HasRows Then
                tableRecord.Read()
                itemName0.Text = tableRecord("item_name").ToString
                price0.Text = tableRecord("price").ToString
                quantity0.Text = tableRecord("quantity").ToString
                total0.Text = tableRecord("total").ToString

                itemTable.AutoSize = True

                While tableRecord.Read()

                    Dim itemName = New TextBox()
                    itemName.Name = "itemName" & itemTable.RowCount - 1
                    itemName.Text = tableRecord("item_name").ToString

                    Dim quantity = New TextBox()
                    quantity.Name = "quantity" & itemTable.RowCount - 1
                    quantity.Text = tableRecord("quantity").ToString
                    AddHandler quantity.TextChanged, AddressOf txtTextChangedItem

                    Dim price = New TextBox()
                    price.Text = tableRecord("price").ToString
                    price.Name = "price" & itemTable.RowCount - 1
                    AddHandler price.TextChanged, AddressOf txtTextChangedItem

                    Dim label = New Label()
                    label.Text = tableRecord("total").ToString
                    label.Name = "total" & itemTable.RowCount - 1

                    itemTable.RowCount += 1
                    itemTable.Controls.Add(itemName, 0, itemTable.RowCount - 1)
                    itemTable.Controls.Add(quantity, 1, itemTable.RowCount - 1)
                    itemTable.Controls.Add(price, 2, itemTable.RowCount - 1)
                    itemTable.Controls.Add(label, 3, itemTable.RowCount - 1)
                End While
            End If
            con.Close()
        End If
    End Sub

    Private Sub continueq_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Hide()
    End Sub

    Private Sub client_TextChanged(sender As Object, e As EventArgs) Handles client.TextChanged

    End Sub

    Private Sub Label5_Click(sender As Object, e As EventArgs) Handles Label5.Click, total0.Click

    End Sub

    Private Sub TableLayoutPanel1_Paint(sender As Object, e As PaintEventArgs) Handles itemTable.Paint

    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles itemName0.TextChanged

    End Sub

    Private Sub addItemRow_Click(sender As Object, e As EventArgs) Handles addItemRow.Click
        itemTable.AutoSize = True
        Dim InsertRowNum = itemTable.RowCount
        itemTable.AutoScroll = True
        itemTable.Visible = True

        itemTable.RowStyles.Add(New RowStyle(SizeType.Absolute, 30))

        Dim itemName = New TextBox()
        itemName.Name = "itemName" & itemTable.RowCount - 1

        Dim quantity = New TextBox()
        quantity.Name = "quantity" & itemTable.RowCount - 1
        AddHandler quantity.TextChanged, AddressOf txtTextChangedItem

        Dim price = New TextBox()
        price.Name = "price" & itemTable.RowCount - 1
        AddHandler price.TextChanged, AddressOf txtTextChangedItem

        Dim label = New Label()
        label.Text = "0"
        label.Name = "total" & itemTable.RowCount - 1

        itemTable.RowCount += 1
        itemTable.Controls.Add(itemName, 0, itemTable.RowCount - 1)
        itemTable.Controls.Add(quantity, 1, itemTable.RowCount - 1)
        itemTable.Controls.Add(price, 2, itemTable.RowCount - 1)
        itemTable.Controls.Add(label, 3, itemTable.RowCount - 1)
    End Sub

    Function checkForInt(val As String)
        Dim num As Integer
        Dim int_OK As Boolean
        Dim index As Integer
        int_OK = Integer.TryParse(val, num)
        If int_OK Then
            index = num
        Else
            index = -1
        End If
        Return index
    End Function


    Private Sub txtTextChangedItem(sender As Object, e As EventArgs)
        Dim txt = DirectCast(sender, TextBox)
        Dim elementName As String = txt.Name
        Dim numCahr = GetChar(elementName, elementName.Length)
        Dim index As Integer = checkForInt(numCahr)

        If (index >= 0) Then
            Dim quantity As Integer
            Dim price As Integer
            Dim total As Integer
            Dim tbox As TextBox = Me.Controls.Find("quantity" & index, True).FirstOrDefault()
            Dim val As Integer = checkForInt(tbox.Text)
            quantity = val
            If val = -1 Then
                quantity = 1
            End If

            tbox = Me.Controls.Find("price" & index, True).FirstOrDefault()
            val = checkForInt(tbox.Text)
            price = val
            If val = -1 Then
                price = 1
            End If

            total = price * quantity
            Dim totalLabel As Label = Me.Controls.Find("total" & index, True).FirstOrDefault()
            totalLabel.Text = total.ToString()
        End If

        'Select Case txt.Name
        ''Case "TextBox1"
        'MsgBox(txt.Text)
        'Case "TextBox2"
        'MsgBox(txt.Text)
        'End Select
    End Sub

    Private Sub price0_TextChanged(sender As Object, e As EventArgs) Handles price0.TextChanged

        Dim num As Integer
        Dim quantity As Integer
        Dim price As Integer

        Dim index As Integer = checkForInt(quantity0.Text)
        quantity = index
        If index = -1 Then
            quantity = 1
        End If

        index = checkForInt(price0.Text)
        price = index
        If index = -1 Then
            price = 1
        End If

        total0.Text = price * quantity
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        'itemTable.RowCount -= 1
        If itemTable.RowCount <= 2 Then
            Return
        End If

        For i As Integer = 0 To itemTable.ColumnCount - 1
            Dim con As Control = itemTable.GetControlFromPosition(i, itemTable.RowCount - 1)
            itemTable.Controls().Remove(con)
        Next

        itemTable.RowCount -= 1
    End Sub

    Function resetForm()
        Me.Controls.Clear()
        InitializeComponent()
    End Function

    Private Sub save_Click(sender As Object, e As EventArgs) Handles save.Click

        Dim connString As String
        connString = ConfigurationManager.ConnectionStrings("connectionString").ConnectionString

        Dim PhoneValid, emailValid As Boolean
        Dim PhoneNumber As String = "^[1-9]\d{2}[1-9]\d{2}\d{4}$"
        Dim emailIdRegEx As String = "^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$"
        Dim ChekPhone As New Regex(PhoneNumber)
        PhoneValid = ChekPhone.IsMatch(phone.Text)

        Dim ChekEmail As New Regex(emailIdRegEx)
        emailValid = ChekEmail.IsMatch(emailid.Text)

        If client.Text = "" Then
            MsgBox("Enter Name")
            Return
        ElseIf DateTimePicker1.Text = "" Then
            MsgBox("Enter booking date")
            Return
        ElseIf venue.Text = "" Then
            MsgBox("Enter Venue")
            Return
        ElseIf PhoneValid <> True Then
            MsgBox("Enter valid phone number")
            Return
        ElseIf emailValid <> True Then
            MsgBox("Enter valid email id")
            Return
        End If

        Dim t As String
        Dim total As Integer
        For r As Integer = 1 To itemTable.RowCount - 1
            t = Me.Controls.Find("total" & (r - 1), True).FirstOrDefault().Text
            total += checkForInt(t)
        Next

        'Connection setting
        Dim bookid As Integer
        Dim con As New SqlConnection
        Dim cmd As New SqlCommand
        con.ConnectionString = connString
        con.Open()

        Try
            cmd.Connection = con
            Dim query As String = String.Empty

            'Save booking detail 
            If bookIdFromOtherForm <> 0 Then
                query &= "UPDATE booking SET client_name = '" & client.Text & "', "
                query &= "venue = '" & venue.Text & "', "
                query &= "date = '" & DateTimePicker1.Text & "', "
                query &= "email = '" & emailid.Text & "', "
                query &= "phone = '" & phone.Text & "', "
                query &= "total_cost = '" & total.ToString() & "' WHERE bookid = " & bookIdFromOtherForm
                cmd = New SqlCommand(query, con)
                cmd.ExecuteNonQuery()
                bookid = bookIdFromOtherForm

                cmd = New SqlCommand("DELETE FROM booking_item WHERE bookid = " & bookIdFromOtherForm, con)
                cmd.ExecuteNonQuery()
            Else
                query &= "INSERT INTO booking (client_name, venue, date, total_cost, email, phone) "
                query &= "VALUES ('" & client.Text & "','" & venue.Text & "','" & DateTimePicker1.Text & "'," & total.ToString() & ", '" & emailid.Text & "', '" & phone.Text & "')"
                query &= "SELECT SCOPE_IDENTITY()"
                cmd = New SqlCommand(query, con)
                bookid = Convert.ToInt32(cmd.ExecuteScalar())
            End If

            Dim totalCost As Integer
            Dim q, p, iname As TextBox
            Dim qv, pv, tc, itemIndex As Integer
            For r As Integer = 1 To itemTable.RowCount - 1

                itemIndex = r - 1
                iname = Me.Controls.Find("itemName" & itemIndex, True).FirstOrDefault()
                q = Me.Controls.Find("quantity" & itemIndex, True).FirstOrDefault()
                p = Me.Controls.Find("price" & itemIndex, True).FirstOrDefault()

                If iname.Text = "" Then
                    MsgBox("Enter all item names")
                    Return
                ElseIf q.Text = "" Then
                    MsgBox("Enter all quantity")
                    Return
                ElseIf p.Text = "" Then
                    MsgBox("Enter all prices")
                    Return
                End If

                qv = checkForInt(q.Text)
                pv = checkForInt(p.Text)

                If qv = -1 Then
                    qv = 1
                End If

                If pv = -1 Then
                    pv = 1
                End If

                tc = qv * pv
                totalCost += tc

                'Prepare for item save
                query = ""
                query &= "INSERT INTO booking_item (bookid, item_name, price, quantity, total) "
                query &= "VALUES (@bookid, @item_name, @price, @quantity, @total)"
                cmd = New SqlCommand(query, con)

                cmd.Parameters.AddWithValue("@bookid", bookid)
                cmd.Parameters.AddWithValue("@item_name", iname.Text)
                cmd.Parameters.AddWithValue("@price", pv)
                cmd.Parameters.AddWithValue("@quantity", qv)
                cmd.Parameters.AddWithValue("@total", tc)
                cmd.ExecuteNonQuery()
            Next

            'Update total if needed
            resetForm()
        Catch ex As Exception
            MsgBox("Error occured: " & ex.Message)
        Finally
            con.Close()
        End Try
    End Sub

End Class