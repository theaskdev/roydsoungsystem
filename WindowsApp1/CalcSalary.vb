﻿Imports System.Configuration
Imports System.Data.SqlClient

Public Class CalcSalary
    Private emp_id As Integer = 0
    Private fromDate, toDate, days, amount As String

    Private Sub CalcSalary_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        pay.Hide()
        Dim con As New SqlConnection
        Dim cmd As New SqlCommand

        con.ConnectionString = ConfigurationManager.ConnectionStrings("connectionString").ConnectionString
        con.Open()
        cmd.Connection = con
        cmd.CommandText = "SELECt * FROM users WHERE role='employee'"
        Dim tableRecord As SqlDataReader = cmd.ExecuteReader()
        If tableRecord.HasRows Then

            Dim comboSource As New Dictionary(Of String, String)()
            While tableRecord.Read()
                comboSource.Add(tableRecord("uid").ToString, tableRecord("name").ToString)
                employee.DataSource = New BindingSource(comboSource, Nothing)
                employee.DisplayMember = "Value"
                employee.ValueMember = "Key"
            End While
        End If
        con.Close()
    End Sub

    Function getSalaryPerDay()
        Dim sal As String = 0

        Dim con As New SqlConnection
        Dim cmd As New SqlCommand
        Dim query As String

        con.ConnectionString = ConfigurationManager.ConnectionStrings("connectionString").ConnectionString
        con.Open()
        cmd.Connection = con

        query = "SELECT value FROM settings WHERE field_name = 'salary'"
        cmd.CommandText = query
        Dim tableRecord As SqlDataReader = cmd.ExecuteReader()
        If tableRecord.HasRows Then
            tableRecord.Read()
            sal = tableRecord(0).ToString
        End If
        con.Close()

        Return sal
    End Function

    Function checkForAlreadyPaid(f, t, eid)
        Dim query As String = "Select * From salary_transaction Where empid = " & eid & " and '" & f & "' between from_date and to_date OR '" & t & "' between from_date and to_date"
        Dim con As New SqlConnection
        Dim cmd As New SqlCommand
        Dim status As Boolean = False
        con.ConnectionString = ConfigurationManager.ConnectionStrings("connectionString").ConnectionString
        con.Open()
        cmd.Connection = con
        cmd.CommandText = query
        Dim tableRecord As SqlDataReader = cmd.ExecuteReader()

        If tableRecord.HasRows Then
            status = True
        End If
        Return status
    End Function

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim empid As String = DirectCast(employee.SelectedItem, KeyValuePair(Of String, String)).Key
        emp_id = empid
        Dim query As String = ""

        If from_date.Text = "" Then
            MsgBox("Select from Date")
            Return
        ElseIf to_date.Text = "" Then
            MsgBox("Select To Date")
            Return
        End If

        fromDate = from_date.Text
        toDate = to_date.Text

        Dim paidSalary As Boolean = checkForAlreadyPaid(fromDate, toDate, empid)
        If paidSalary Then
            MsgBox("Salary already paid for give date range. Select Proper date range")
            Return
        End If
        query = "Select COUNT(*) FROM attendance "
        query &= " where empid=" & empid & " And status='P' and date between convert(datetime, '" & from_date.Text & "') and convert(datetime, '" & to_date.Text & "')"

        Dim con As New SqlConnection
        Dim cmd As New SqlCommand

        con.ConnectionString = ConfigurationManager.ConnectionStrings("connectionString").ConnectionString
        con.Open()
        cmd.Connection = con
        cmd.CommandText = query
        Dim tableRecord As SqlDataReader = cmd.ExecuteReader()

        If tableRecord.HasRows Then
            tableRecord.Read()
            Dim daysWorked As String = tableRecord(0).ToString
            If daysWorked <> 0 Then
                days = daysWorked
                Dim salary_per_day As String = getSalaryPerDay()
                Dim totalSalary As String = salary_per_day * daysWorked
                amount = totalSalary
                total_salary.Text = totalSalary
                If totalSalary <> 0 Then
                    pay.Show()
                Else
                    pay.Hide()
                End If
            Else
                MsgBox("No record to calculate salary")
            End If

        Else
                pay.Hide()
        End If

    End Sub

    Private Sub pay_Click(sender As Object, e As EventArgs) Handles pay.Click
        Dim con As New SqlConnection
        Dim cmd As New SqlCommand
        Dim query As String

        con.ConnectionString = ConfigurationManager.ConnectionStrings("connectionString").ConnectionString
        con.Open()
        cmd.Connection = con

        query = "INSERT INTO salary_transaction (empid, from_date, to_date, days, amount, date) "
        query &= "VALUES ('" & emp_id & "','" & fromDate & "','" & toDate & "'," & days & "," & amount & ", GETDATE())"
        cmd.CommandText = query
        cmd.ExecuteNonQuery()
        con.Close()

        MsgBox("Salary payment is done.")
        Me.Close()
    End Sub
End Class