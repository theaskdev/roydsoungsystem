﻿Imports System.Configuration
Imports System.Data.SqlClient

Public Class AttendanceReport

    Private Sub load_report_Click(sender As Object, e As EventArgs) Handles load_report.Click
        Dim empid As String = DirectCast(employee.SelectedItem, KeyValuePair(Of String, String)).Key

        Dim from_date As String = date_from.Text
        Dim to_date As String = date_to.Text

        If from_date = "" Or to_date = "" Then
            MsgBox("Select from and to date")
            Return
        End If

        report_table.AutoSize = True

        report_table.Controls.Clear()
        report_table.ColumnStyles.Clear()
        report_table.RowStyles.Clear()

        Dim fd As Date = from_date
        Dim td As Date = to_date
        Dim dateRangeDiff As Integer = DateDiff(DateInterval.Day, fd, td)

        If dateRangeDiff > 0 Then

            Dim con As New SqlConnection
            Dim cmd As New SqlCommand

            con.ConnectionString = ConfigurationManager.ConnectionStrings("connectionString").ConnectionString
            con.Open()

            cmd.Connection = con
            cmd.CommandText = "SELECT * FROM attendance WHERE empid = " & empid & " and date between '" & from_date & "' and '" & to_date & "'"
            'MsgBox(cmd.CommandText)
            Dim tableRecord As SqlDataReader = cmd.ExecuteReader()

            Dim i As Integer
            If tableRecord.HasRows Then
                report_table.ColumnCount = 2
                report_table.RowCount = 2

                'Generate table header
                Dim header = New String() {"Date", "Status"}

                For i = 0 To header.Length - 1
                    report_table.ColumnStyles.Add(New ColumnStyle(SizeType.AutoSize))
                    Dim lbl As Label = New Label()
                    lbl.Text = header(i)
                    report_table.Controls.Add(lbl, i, 0)
                Next

                While tableRecord.Read()
                    report_table.RowStyles.Add(New RowStyle(SizeType.AutoSize))
                    report_table.ColumnStyles.Add(New ColumnStyle(SizeType.AutoSize))

                    Dim lbl1 As Label = New Label()
                    lbl1.Text = tableRecord("date").ToString
                    report_table.Controls.Add(lbl1, 0, report_table.RowCount)

                    Dim lbl2 As Label = New Label()
                    lbl2.Text = tableRecord("status").ToString
                    report_table.Controls.Add(lbl2, 1, report_table.RowCount)

                    report_table.RowCount += 1
                End While
            Else
                MsgBox("No records found")
            End If
        Else
            MsgBox("Select proper Date range")
        End If
    End Sub

    Private Sub AttendanceReport_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim con As New SqlConnection
        Dim cmd As New SqlCommand

        con.ConnectionString = ConfigurationManager.ConnectionStrings("connectionString").ConnectionString
        con.Open()
        cmd.Connection = con
        cmd.CommandText = "SELECt * FROM users WHERE role='employee'"
        Dim tableRecord As SqlDataReader = cmd.ExecuteReader()
        If tableRecord.HasRows Then

            Dim comboSource As New Dictionary(Of String, String)()
            While tableRecord.Read()
                comboSource.Add(tableRecord("uid").ToString, tableRecord("name").ToString)
                employee.DataSource = New BindingSource(comboSource, Nothing)
                employee.DisplayMember = "Value"
                employee.ValueMember = "Key"
            End While
        End If
        con.Close()
    End Sub
End Class