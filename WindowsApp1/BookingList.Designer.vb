﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BookingList
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.itemDetail = New System.Windows.Forms.TableLayoutPanel()
        Me.FillByToolStrip = New System.Windows.Forms.ToolStrip()
        Me.PulsesoundsDataSet1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BookingBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BookingListTable = New System.Windows.Forms.TableLayoutPanel()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.PulsesoundsDataSet1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BookingBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'itemDetail
        '
        Me.itemDetail.ColumnCount = 2
        Me.itemDetail.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.itemDetail.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.itemDetail.Location = New System.Drawing.Point(32, 278)
        Me.itemDetail.Name = "itemDetail"
        Me.itemDetail.RowCount = 2
        Me.itemDetail.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.itemDetail.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.itemDetail.Size = New System.Drawing.Size(712, 90)
        Me.itemDetail.TabIndex = 2
        '
        'FillByToolStrip
        '
        Me.FillByToolStrip.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.FillByToolStrip.Location = New System.Drawing.Point(0, 0)
        Me.FillByToolStrip.Name = "FillByToolStrip"
        Me.FillByToolStrip.Size = New System.Drawing.Size(1061, 25)
        Me.FillByToolStrip.TabIndex = 3
        Me.FillByToolStrip.Text = "FillByToolStrip"
        '
        'PulsesoundsDataSet1
        ''
        'PulsesoundsDataSet1BindingSource
        '
        Me.PulsesoundsDataSet1BindingSource.Position = 0
        '
        'BookingBindingSource
        '
        Me.BookingBindingSource.DataMember = "booking"
        '
        'PulsesoundsDataSet
        ' '
        'BookingTableAdapter
        '
        '
        'BookingListTable
        '
        Me.BookingListTable.ColumnCount = 2
        Me.BookingListTable.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.BookingListTable.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.BookingListTable.Location = New System.Drawing.Point(32, 28)
        Me.BookingListTable.Name = "BookingListTable"
        Me.BookingListTable.RowCount = 2
        Me.BookingListTable.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.BookingListTable.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.BookingListTable.Size = New System.Drawing.Size(712, 117)
        Me.BookingListTable.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(925, 438)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 17)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Label1"
        '
        'BookingList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1061, 467)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.BookingListTable)
        Me.Controls.Add(Me.FillByToolStrip)
        Me.Controls.Add(Me.itemDetail)
        Me.Name = "BookingList"
        Me.Text = "BookingList"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.PulsesoundsDataSet1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BookingBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BookingBindingSource As BindingSource
    Friend WithEvents itemDetail As TableLayoutPanel
    Friend WithEvents FillByToolStrip As ToolStrip
    Friend WithEvents PulsesoundsDataSet1BindingSource As BindingSource
    Friend WithEvents BookingListTable As TableLayoutPanel
    Friend WithEvents Label1 As Label
End Class
