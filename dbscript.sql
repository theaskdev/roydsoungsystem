USE [master]
GO
/****** Object:  Database [pulsesounds]    Script Date: 24-Mar-19 6:52:39 PM ******/
CREATE DATABASE [pulsesounds]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'pulsesounds', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\pulsesounds.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'pulsesounds_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\pulsesounds_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [pulsesounds] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [pulsesounds].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [pulsesounds] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [pulsesounds] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [pulsesounds] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [pulsesounds] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [pulsesounds] SET ARITHABORT OFF 
GO
ALTER DATABASE [pulsesounds] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [pulsesounds] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [pulsesounds] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [pulsesounds] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [pulsesounds] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [pulsesounds] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [pulsesounds] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [pulsesounds] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [pulsesounds] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [pulsesounds] SET  DISABLE_BROKER 
GO
ALTER DATABASE [pulsesounds] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [pulsesounds] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [pulsesounds] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [pulsesounds] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [pulsesounds] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [pulsesounds] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [pulsesounds] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [pulsesounds] SET RECOVERY FULL 
GO
ALTER DATABASE [pulsesounds] SET  MULTI_USER 
GO
ALTER DATABASE [pulsesounds] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [pulsesounds] SET DB_CHAINING OFF 
GO
ALTER DATABASE [pulsesounds] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [pulsesounds] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [pulsesounds] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'pulsesounds', N'ON'
GO
ALTER DATABASE [pulsesounds] SET QUERY_STORE = OFF
GO
USE [pulsesounds]
GO
/****** Object:  Table [dbo].[attendance]    Script Date: 24-Mar-19 6:52:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[attendance](
	[aid] [int] IDENTITY(1,1) NOT NULL,
	[empid] [int] NOT NULL,
	[date] [nvarchar](150) NULL,
	[status] [nvarchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[booking]    Script Date: 24-Mar-19 6:52:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[booking](
	[bookid] [int] IDENTITY(1,1) NOT NULL,
	[client_name] [nvarchar](225) NULL,
	[venue] [nvarchar](255) NULL,
	[date] [nvarchar](255) NULL,
	[total_cost] [int] NULL,
	[status] [nvarchar](255) NULL,
	[email] [nvarchar](50) NULL,
	[phone] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[booking_item]    Script Date: 24-Mar-19 6:52:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[booking_item](
	[iid] [int] IDENTITY(1,1) NOT NULL,
	[bookid] [int] NULL,
	[item_name] [nvarchar](50) NULL,
	[price] [int] NULL,
	[quantity] [int] NULL,
	[total] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[salary_transaction]    Script Date: 24-Mar-19 6:52:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[salary_transaction](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[empid] [int] NULL,
	[from_date] [nvarchar](50) NULL,
	[to_date] [nvarchar](50) NULL,
	[days] [int] NULL,
	[amount] [nvarchar](50) NULL,
	[date] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[settings]    Script Date: 24-Mar-19 6:52:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[settings](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[field_name] [nvarchar](100) NULL,
	[value] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[users]    Script Date: 24-Mar-19 6:52:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users](
	[uid] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NULL,
	[username] [nvarchar](50) NOT NULL,
	[password] [nvarchar](255) NOT NULL,
	[role] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_users] PRIMARY KEY CLUSTERED 
(
	[uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[attendance] ON 

INSERT [dbo].[attendance] ([aid], [empid], [date], [status]) VALUES (2004, 8, N'21-Mar-19', N'P')
INSERT [dbo].[attendance] ([aid], [empid], [date], [status]) VALUES (2005, 8, N'19-Mar-19', N'A')
INSERT [dbo].[attendance] ([aid], [empid], [date], [status]) VALUES (2006, 10, N'20-Mar-19', N'P')
SET IDENTITY_INSERT [dbo].[attendance] OFF
SET IDENTITY_INSERT [dbo].[booking] ON 

INSERT [dbo].[booking] ([bookid], [client_name], [venue], [date], [total_cost], [status], [email], [phone]) VALUES (53, N'some clients', N'sdfsd', N'Friday, March 22, 2019', 1422, N'invoice', NULL, NULL)
INSERT [dbo].[booking] ([bookid], [client_name], [venue], [date], [total_cost], [status], [email], [phone]) VALUES (54, N'asdas', N'asdas', N'Thursday, March 28, 2019', 1332, N'invoice', N'shashi@gadd.com', N'2234323432')
INSERT [dbo].[booking] ([bookid], [client_name], [venue], [date], [total_cost], [status], [email], [phone]) VALUES (1053, N'some', N'sdsdf', N'Friday, March 22, 2019', 12, N'booking', N'asd@gmail.com', N'9986246773')
SET IDENTITY_INSERT [dbo].[booking] OFF
SET IDENTITY_INSERT [dbo].[booking_item] ON 

INSERT [dbo].[booking_item] ([iid], [bookid], [item_name], [price], [quantity], [total]) VALUES (67, 53, N'dadasd', 100, 12, 1200)
INSERT [dbo].[booking_item] ([iid], [bookid], [item_name], [price], [quantity], [total]) VALUES (68, 53, N'asd', 222, 1, 222)
INSERT [dbo].[booking_item] ([iid], [bookid], [item_name], [price], [quantity], [total]) VALUES (1069, 54, N'asd', 111, 12, 1332)
INSERT [dbo].[booking_item] ([iid], [bookid], [item_name], [price], [quantity], [total]) VALUES (1067, 1053, N'asda', 1, 12, 12)
SET IDENTITY_INSERT [dbo].[booking_item] OFF
SET IDENTITY_INSERT [dbo].[salary_transaction] ON 

INSERT [dbo].[salary_transaction] ([id], [empid], [from_date], [to_date], [days], [amount], [date]) VALUES (1002, 8, N'14-Mar-19', N'22-Mar-19', 1, N'1202', N'Mar 22 2019 11:05PM')
SET IDENTITY_INSERT [dbo].[salary_transaction] OFF
SET IDENTITY_INSERT [dbo].[settings] ON 

INSERT [dbo].[settings] ([id], [field_name], [value]) VALUES (1, N'salary', N'1202')
SET IDENTITY_INSERT [dbo].[settings] OFF
SET IDENTITY_INSERT [dbo].[users] ON 

INSERT [dbo].[users] ([uid], [name], [username], [password], [role]) VALUES (7, N'Roydon', N'admin', N'admin123', N'admin')
INSERT [dbo].[users] ([uid], [name], [username], [password], [role]) VALUES (8, N'Emp1', N'emp1', N'emp123', N'employee')
INSERT [dbo].[users] ([uid], [name], [username], [password], [role]) VALUES (9, N'emp1', N'emp', N'emp123', N'employee')
INSERT [dbo].[users] ([uid], [name], [username], [password], [role]) VALUES (10, N'emp2', N'emp2', N'emp123', N'employee')
SET IDENTITY_INSERT [dbo].[users] OFF
ALTER TABLE [dbo].[booking] ADD  CONSTRAINT [DF_booking_status]  DEFAULT (N'booking') FOR [status]
GO
ALTER TABLE [dbo].[users]  WITH NOCHECK ADD  CONSTRAINT [FK_users_users] FOREIGN KEY([uid])
REFERENCES [dbo].[users] ([uid])
GO
ALTER TABLE [dbo].[users] CHECK CONSTRAINT [FK_users_users]
GO
USE [master]
GO
ALTER DATABASE [pulsesounds] SET  READ_WRITE 
GO
